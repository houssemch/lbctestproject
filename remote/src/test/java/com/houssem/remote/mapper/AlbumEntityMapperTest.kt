package com.houssem.remote.mapper

import com.houssem.remote.model.AlbumJsonModel
import org.junit.Assert.assertEquals
import org.junit.Test

class AlbumEntityMapperTest {

    private val mapper: AlbumEntityMapper = AlbumEntityMapper()

    @Test
    fun `map Album to AlbumEntity object should return AlbumEntity`() {
        val albumJsonModel = AlbumJsonModel(
            albumId = 12,
            id = 1,
            title = "title1",
            url = "www.url_test.com",
            thumbnailUrl = "www.thumbnail_url.com"
        )
        val albumEntity = mapper.mapFromModel(albumJsonModel)
        assertEquals(albumEntity.id, 1)
        assertEquals(albumEntity.title, "title1")
        assertEquals(albumEntity.thumbnailUrl, "www.thumbnail_url.com")
    }
}

