package com.houssem.remote.utils

import android.content.Context
import android.content.pm.ApplicationInfo
import android.content.pm.PackageInfo
import android.content.pm.PackageManager
import org.junit.Assert.assertEquals
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.ArgumentMatchers.any
import org.mockito.ArgumentMatchers.eq
import org.mockito.Mock
import org.mockito.Mockito.`when`
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class UserAgentProviderTest {

    @Mock
    lateinit var context: Context

    @Mock
    lateinit var packageManager: PackageManager

    @Test
    fun `test get user agent`() {
        val applicationInfo = ApplicationInfo()
        val packageInfo = PackageInfo()
        packageInfo.versionName = "Version Name"

        `when`(context.packageManager).thenReturn(packageManager)
        `when`(context.packageName).thenReturn("com.houssem.lbctest")
        `when`(
            packageManager.getApplicationInfo(
                any(),
                eq(0)
            )
        ).thenReturn(applicationInfo)
        `when`(packageManager.getApplicationLabel(eq(applicationInfo))).thenReturn(
            "label name"
        )
        `when`(
            packageManager.getPackageInfo(
                eq("com.houssem.lbctest"),
                eq(0)
            )
        )
            .thenReturn(packageInfo)

        assertEquals(
            "Android null label name/Version Name",
            UserAgentProvider(context).userAgent
        )
    }
}