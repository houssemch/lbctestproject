package com.houssem.remote.repository

import com.houssem.data.model.AlbumEntity
import com.houssem.remote.api.AlbumsService
import com.houssem.remote.mapper.AlbumEntityMapper
import com.houssem.remote.model.AlbumJsonModel
import com.houssem.remote.rule.RemoteDispatcherRule
import kotlinx.coroutines.test.runTest
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito.times
import org.mockito.Mockito.verify
import org.mockito.Mockito.`when`
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class AlbumRemoteImpTest {

    @get:Rule
    val dispatcherRule = RemoteDispatcherRule()

    @Mock
    lateinit var albumService: AlbumsService

    @Mock
    lateinit var albumEntityMapper: AlbumEntityMapper

    private lateinit var albumRemote: AlbumRemoteImp

    @Before
    fun setUp() {
        albumRemote = AlbumRemoteImp(albumService, albumEntityMapper)
    }

    @Test
    fun `get albums should return response from remote server`() = runTest {
        `when`(albumService.getAlbums()).thenReturn(testAlbumsJsonModel)
        testAlbumsJsonModel.forEach {
            `when`(albumEntityMapper.mapFromModel(it)).thenReturn(
                AlbumEntity(it.id, it.title, it.thumbnailUrl)
            )
        }

        val albums = albumRemote.getAlbums()

        assertEquals(3, albums.size)
        assertEquals(12, albums[0].id)
        assertEquals("Album title 1", albums[0].title)
        assertEquals("www.fake-thumbnail-url-1.com", albums[0].thumbnailUrl)
        testAlbumsJsonModel.forEach {
            verify(albumEntityMapper, times(1)).mapFromModel(it)
        }
    }
}

private val testAlbumsJsonModel = listOf(
    AlbumJsonModel(1, 12, "Album title 1", "www.fake-url-1.com", "www.fake-thumbnail-url-1.com"),
    AlbumJsonModel(2, 13, "Album title 2", "www.fake-url-2.com", "www.fake-thumbnail-url-1.com"),
    AlbumJsonModel(3, 14, "Album title 3", "www.fake-url-3.com", "www.fake-thumbnail-url-1.com"),
)