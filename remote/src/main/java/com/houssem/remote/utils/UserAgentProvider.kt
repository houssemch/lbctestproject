package com.houssem.remote.utils

import android.content.Context
import android.os.Build

/**
 * A user agent provider based on the given context
 * @param context The application context
 */
class UserAgentProvider(private val context: Context) {

    private val packageManager = context.packageManager
    private val packageName = context.packageName
    private val osVersion = Build.VERSION.RELEASE
    private val appName = packageManager.getApplicationLabel(packageManager.getApplicationInfo(packageName, 0))
    private val appVersionName = packageManager.getPackageInfo(packageName, 0).versionName

    // A user agent build with the android version, the application's name and its version
    val userAgent = "Android $osVersion $appName/$appVersionName"
}