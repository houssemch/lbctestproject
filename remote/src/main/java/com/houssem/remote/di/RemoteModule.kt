package com.houssem.remote.di

import com.houssem.data.repository.AlbumRemote
import com.houssem.remote.repository.AlbumRemoteImp
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object RemoteModule {

    @Provides
    @Singleton
    fun provideAlbumRemote(albumRemote : AlbumRemoteImp): AlbumRemote {
        return albumRemote
    }
}