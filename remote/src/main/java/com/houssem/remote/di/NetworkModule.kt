package com.houssem.remote.di

import android.content.Context
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.houssem.remote.api.AlbumsService
import com.houssem.remote.utils.UserAgentProvider
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object NetworkModule {

    private const val LBC_ALBUMS_BASE_URL = "https://static.leboncoin.fr/"
    private const val READ_TIME_OUT = 30L
    private const val CONNECT_TIME_OUT = 30L

    @Singleton
    @Provides
    fun providesHttpClient(userAgentProvider: UserAgentProvider): OkHttpClient {
        val okHttpBuilder = OkHttpClient.Builder()

        // Logging interceptor
        val loggingInterceptor = HttpLoggingInterceptor().apply {
            level = HttpLoggingInterceptor.Level.BODY

        }

        // Header interceptor
        val headerInterceptor = Interceptor { chain ->
            //return response
            chain.proceed(
                //create request
                chain.request()
                    .newBuilder()
                    //add headers to the request builder
                    .also {
                        it.addHeader("User-Agent", userAgentProvider.userAgent)
                    }
                    .build()
            )
        }
        okHttpBuilder.apply {
            addInterceptor(loggingInterceptor)
            addInterceptor(headerInterceptor)
            readTimeout(READ_TIME_OUT, TimeUnit.SECONDS)
            connectTimeout(CONNECT_TIME_OUT, TimeUnit.SECONDS)
        }

        return okHttpBuilder.build()
    }

    @Singleton
    @Provides
    fun providesGsonBuilder(): Gson {
        return GsonBuilder()
            .setLenient()
            .create()
    }

    @Singleton
    @Provides
    fun providesDefaultRetrofit(
        gson: Gson,
        okHttpClient: OkHttpClient
    ): Retrofit {
        return Retrofit.Builder()
            .baseUrl(LBC_ALBUMS_BASE_URL)
            .client(okHttpClient)
            .addConverterFactory(GsonConverterFactory.create(gson))
            .build()
    }

    @Singleton
    @Provides
    fun providesAlbumsApiService(retrofit: Retrofit): AlbumsService {
        return retrofit.create(AlbumsService::class.java)
    }

    @Provides
    @Singleton
    fun provideUserAgentProvider(@ApplicationContext context: Context): UserAgentProvider {
        return UserAgentProvider(context)
    }
}
