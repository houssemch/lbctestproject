package com.houssem.remote.api

import com.houssem.remote.model.AlbumJsonModel
import retrofit2.http.GET

interface AlbumsService {

    @GET("img/shared/technical-test.json")
    suspend fun getAlbums(): List<AlbumJsonModel>
}

