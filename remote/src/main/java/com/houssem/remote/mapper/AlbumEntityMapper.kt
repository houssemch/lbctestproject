package com.houssem.remote.mapper

import com.houssem.data.model.AlbumEntity
import com.houssem.remote.model.AlbumJsonModel
import javax.inject.Inject

class AlbumEntityMapper @Inject constructor() {

    fun mapFromModel(albumJsonModel: AlbumJsonModel): AlbumEntity {
        return with(albumJsonModel) {
            AlbumEntity(
                id = id,
                title = title,
                thumbnailUrl = thumbnailUrl
            )
        }
    }
}
