package com.houssem.remote.repository

import com.houssem.data.model.AlbumEntity
import com.houssem.data.repository.AlbumRemote
import com.houssem.remote.api.AlbumsService
import com.houssem.remote.mapper.AlbumEntityMapper
import javax.inject.Inject

/**
 * Remote module class that implements [AlbumRemote] data layer interface,
 * that retrieves data from [AlbumsService].
 */
class AlbumRemoteImp @Inject constructor(
    private val albumService: AlbumsService,
    private val albumEntityMapper: AlbumEntityMapper
) : AlbumRemote {

    override suspend fun getAlbums(): List<AlbumEntity> {
        return albumService.getAlbums().map { albumModel ->
            albumEntityMapper.mapFromModel(albumModel)
        }
    }
}
