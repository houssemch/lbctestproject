package com.houssem.lbctest.di

import com.houssem.data.mapper.AlbumMapper
import com.houssem.data.AlbumsRepositoryImpl
import com.houssem.data.source.AlbumDataSourceFactory
import com.houssem.domain.repository.AlbumsRepository
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object DataModule {

    @Singleton
    @Provides
    fun providesAlbumsRepository(
        albumDataSourceFactory: AlbumDataSourceFactory,
        albumMapper: AlbumMapper
    ): AlbumsRepository {
        return AlbumsRepositoryImpl(
            dataSourceFactory = albumDataSourceFactory,
            albumMapper = albumMapper
        )
    }

    @Singleton
    @Provides
    fun providesAlbumMapper() = AlbumMapper()

}