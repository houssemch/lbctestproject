package com.houssem.lbctest.di

import com.houssem.domain.model.Album
import com.houssem.domain.repository.AlbumsRepository
import com.houssem.domain.usecase.GetAlbumsUseCase
import com.houssem.domain.usecase.UseCase
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object UseCaseModule {

    @Singleton
    @Provides
    fun provideGetAlbumsUseCase(albumsRepository: AlbumsRepository): UseCase<List<Album>> {
        return GetAlbumsUseCase(albumsRepository)
    }

    @Singleton
    @Provides
    fun provideCoroutineDispatcher(): CoroutineDispatcher {
        return Dispatchers.IO
    }
}