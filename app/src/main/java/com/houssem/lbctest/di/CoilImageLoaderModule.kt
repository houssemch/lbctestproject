package com.houssem.lbctest.di

import android.content.Context
import coil.ImageLoader
import coil.disk.DiskCache
import coil.memory.MemoryCache
import coil.request.CachePolicy
import coil.util.DebugLogger
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
class CoilImageLoaderModule {
    @Provides
    @Singleton
    fun imageLoader(
        @ApplicationContext application: Context,
    ): ImageLoader = ImageLoader.Builder(application)
        .memoryCachePolicy(CachePolicy.ENABLED)
        .memoryCache {
            MemoryCache.Builder(application)
                .maxSizePercent(MAX_SIZE_PERCENT)
                .build()
        }
        .diskCachePolicy(CachePolicy.ENABLED)
        .diskCache {
            DiskCache.Builder()
                .directory(application.cacheDir.resolve(IMAGE_DIR_NAME))
                .maxSizeBytes(MAX_SIZE_BYTES)
                .build()
        }
        .logger(DebugLogger())
        .respectCacheHeaders(false)
        .build()

    private companion object {
        const val IMAGE_DIR_NAME = "image_cache"
        const val MAX_SIZE_PERCENT = 0.20
        const val MAX_SIZE_BYTES = 5 * 1024 * 1024L
    }
}