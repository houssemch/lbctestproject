package com.houssem.lbctest.di

import android.content.Context
import com.houssem.data.repository.AlbumCache
import com.houssem.database.AlbumCacheImpl
import com.houssem.database.dao.AlbumDao
import com.houssem.database.database.AlbumsDatabase
import com.houssem.database.utils.PreferencesHelper
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
class DatabaseModule {

    @Provides
    @Singleton
    fun provideRoomDatabase(@ApplicationContext context: Context): AlbumsDatabase {
        return AlbumsDatabase.getInstance(context)
    }

    @Provides
    @Singleton
    fun provideAlbumDao(albumsDatabase: AlbumsDatabase): AlbumDao {
        return albumsDatabase.cachedAlbumDao()
    }

    @Provides
    @Singleton
    fun provideAlbumCache(albumCache: AlbumCacheImpl): AlbumCache {
        return albumCache
    }

    @Provides
    @Singleton
    fun providePreferenceHelper(@ApplicationContext context: Context): PreferencesHelper {
        return PreferencesHelper(context)
    }
}