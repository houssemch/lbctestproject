package com.houssem.lbctest

import android.app.Application
import coil.ImageLoader
import coil.ImageLoaderFactory
import dagger.hilt.android.HiltAndroidApp
import javax.inject.Inject
import javax.inject.Provider

@HiltAndroidApp
class LbcTestApplication : Application(), ImageLoaderFactory {

    @Inject
    lateinit var imageLoader: Provider<ImageLoader>

    /**
     * override the Coil image loader to set memoryCache and diskCache configurations
     */
    override fun newImageLoader(): ImageLoader = imageLoader.get()
}