# LbcTest

LbcTest is a technical test app, built with [Kotlin](https://kotlinlang.org/) and [Jetpack Compose](https://developer.android.com/jetpack/compose).
It's consuming an [Albums info API](https://static.leboncoin.fr/img/shared/technical-test.json) to display a list of Albums and to save it in a [Room database](https://developer.android.com/jetpack/androidx/releases/room) to support the offline mode.

## Screenshots
<img src="screenshots/albums_portrait.jpg" alt="Screenshot" width="360" height="804">
<img src="screenshots/items_loading.jpg" alt="Screenshot" width="360" height="804">
<img src="screenshots/albums_landscape.jpg" alt="Screenshot" width="804" height="360">
<img src="screenshots/no_network.jpg" alt="Screenshot" width="360" height="804">
<img src="screenshots/loading.jpg" alt="Screenshot" width="360" height="804">

## Architecture
The application was developed following the principles of Clean Architecture and Unidirectional Data Flow (UDF), while also adhering to the MVVM design pattern:

**The Choice of using Clean Architecture**

The project was structured into multiple layers (an app module with 5 other modules) with the aim of ensuring a clear separation of responsibilities, aach layer has a specific role in the process of retrieving and displaying the albums.

This way, from each module, access to classes contained in another module can only be done through an explicit addition of dependency to the latter, this approach promotes modular design and avoids unwanted dependencies.

The integration of Android modules was also motivated by the goal of improving the project's compilation time, in order to optimize overall development efficiency, while ensuring a clear and scalable architecture.


**The Choice of using Unidirectional Data Flow (UDF) and MVVM**

Data follows a single direction to be transferred to different parts of the application, each event emitted by the UI (for example, opening the Albums screen after launching the application) is first processed by the ViewModel and then propagated to other components of the application, on the other hand, "State flows" circulate in the opposite direction to update the state stored at the ViewModel level.

The UI layer relies exclusively on the ViewModel to receive the states to be displayed, making the ViewModel the single source of truth. Thus, the ViewModel plays a central role in managing and distributing data within the application, ensuring consistency and reliability of the information presented to the user.

## Modules:
- **app:**
  This module serves as the entry point to the application, using components and classes related to the Android framework. It includes the Application class (LbcTestApplication) and the Launcher Activity (MainActivity).
  It defines the application theme and launchs the display of the UI part implemented in the Presentation module.
  It has access to all modules and manages dependency injection across them.

- **presentation:**
  This module is based on MVVM Design Pattern with a ViewModel (AlbumsListViewModel) that exposes the StateFlow consumed by the UI.
  It contains the implementation of the UI part through Composable functions and includes a "designsystem" package to centralize Composable functions used by the UI part.
  This module only has access to the "domain" module.

- **domain:**
  It is a Kotlin module that cannot access any other module, It contains the UseCase responsible for managing the business logic for a specific case, which here involves retrieving lists of albums from the data source (either remote or local).
  It defines the Repository, which is later implemented by the data layer to retrieve the data.

- **data:**
  This layer implements the Repository defined by the Domain layer. It provides a single source for data retrieval by defining an AlbumDataSource interface, which is implemented by both the Remote and Database layers.
  It also handles through a Mapper (AlbumMapper) the converting of data retrieved by Remote and Database layers into objects that will be exposed to Domain layer.

- **remote:**
  This module can only access the data module, It implements the AlbumRemote interface defined by the Data layer to retrieve remote data, this is done by calling a REST API using the Retrofit library.
  It contains the definition of JSON models defined by the API and a Mapper (AlbumEntityMapper) that converts these models into objects consumed by the Data module.

- **database**:
  This module can only access the data module, It implements the AlbumCache interface defined by the Data layer to manage interactions with the local database (Room Database).
  The data model for this layer is the AlbumCacheEntity, which is converted to and from an AlbumEntity instance from the data layer using the AlbumCacheMapper class.



## Tech stack - Libraries:

- **Jetpack Compose:** Used for creating the UI in the application. It was chosen because it simplifies and accelerates the UI creation process and automatically manages UI updates in response to state changes from the ViewModel.
- **Coroutines:** Used for handling asynchronously executed code. It enables simplified asynchronous code by automating thread management, resulting in improved performance.
- **Flow:** Used to efficiently handle asynchronous data streams in the application and to support their transformation, as well as error handling.
- **Retrofit:** Used for managing interactions with the REST API.
- **OkHttp:** Used for setting the HTTP client used by Retrofit to make network calls. It allowed adding interceptors at the API call level to support logging and adding Header (User-Agent).
- **Gson:** Used for converting JSON data returned by HTTP requests into Kotlin objects.
- **Hilt:** Used for dependency injection management in the application.
- **Room:** Used for creating a Room database to store the list of albums retrieved from the API.
- **Coil:** Used for loading an image from the URL in each album list item on the Compose screen.
- **Version Catalog:** Used for managing versions of libraries and dependencies used across all modules in a centralized manner.
- **JUnit:** Used for implementing unit tests in the project.
- **Mockito:**:Used for mocking objects and behaviors in unit tests.
- **Kotlinx Coroutines Test:** Used in unit tests to facilitate testing functions that use coroutines, allowing reliable and predictable control over their execution.
- **Turbine:** Used to facilitate unit testing of Kotlin Flows in the application.
