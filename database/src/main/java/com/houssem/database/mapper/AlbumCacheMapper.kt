package com.houssem.database.mapper

import com.houssem.data.model.AlbumEntity
import com.houssem.database.models.AlbumCacheEntity
import javax.inject.Inject

class AlbumCacheMapper @Inject constructor() : CacheMapper<AlbumCacheEntity, AlbumEntity> {
    override fun mapFromCached(type: AlbumCacheEntity): AlbumEntity {
        return with(type) {
            AlbumEntity(
                id = id,
                title = title,
                thumbnailUrl = thumbnailUrl
            )
        }
    }

    override fun mapToCached(type: AlbumEntity): AlbumCacheEntity {
        return with(type) {
            AlbumCacheEntity(
                id = id,
                title = title,
                thumbnailUrl = thumbnailUrl
            )
        }
    }
}