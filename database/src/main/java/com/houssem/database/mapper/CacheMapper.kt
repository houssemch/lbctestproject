package com.houssem.database.mapper

/**
 * Base mapper interface for converting from and to Cache object.
 */
interface CacheMapper<T, V> {

    fun mapFromCached(type: T): V

    fun mapToCached(type: V): T
}