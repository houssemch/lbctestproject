package com.houssem.database.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.houssem.database.models.AlbumCacheEntity

@Dao
interface AlbumDao {

    @Query("SELECT * FROM albums")
    fun getAlbums(): List<AlbumCacheEntity>

    @Query("DELETE FROM albums")
    fun clearAlbums(): Int

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun addAlbum(vararg album: AlbumCacheEntity)
}
