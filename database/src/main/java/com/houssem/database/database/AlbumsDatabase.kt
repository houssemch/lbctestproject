package com.houssem.database.database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.houssem.database.dao.AlbumDao
import com.houssem.database.models.AlbumCacheEntity
import com.houssem.database.utils.DatabaseConstants
import javax.inject.Inject

@Database(
    entities = [AlbumCacheEntity::class],
    version = 1,
    exportSchema = false
)
abstract class AlbumsDatabase @Inject constructor() : RoomDatabase() {

    abstract fun cachedAlbumDao(): AlbumDao

    companion object {
        @Volatile
        private var INSTANCE: AlbumsDatabase? = null

        fun getInstance(context: Context): AlbumsDatabase = INSTANCE ?: synchronized(this) {
            INSTANCE ?: buildDatabase(context).also { INSTANCE = it }
        }

        private fun buildDatabase(context: Context) = Room.databaseBuilder(
            context.applicationContext,
            AlbumsDatabase::class.java,
            DatabaseConstants.DB_NAME
        ).build()
    }
}