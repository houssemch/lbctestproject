package com.houssem.database.utils

object DatabaseConstants {
    const val DB_NAME = "albums_lbc.db"
    const val ALBUM_TABLE_NAME = "albums"
}