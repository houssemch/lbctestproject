package com.houssem.database

import com.houssem.data.model.AlbumEntity
import com.houssem.data.repository.AlbumCache
import com.houssem.database.dao.AlbumDao
import com.houssem.database.mapper.AlbumCacheMapper
import com.houssem.database.utils.PreferencesHelper
import javax.inject.Inject

class AlbumCacheImpl @Inject constructor(
    private val albumDao: AlbumDao,
    private val albumCacheMapper: AlbumCacheMapper,
    private val preferencesHelper: PreferencesHelper
) : AlbumCache {
    override suspend fun getAlbums(): List<AlbumEntity> {
        return albumDao.getAlbums().map { cacheAlbum ->
            albumCacheMapper.mapFromCached(cacheAlbum)
        }
    }

    override suspend fun saveAlbums(listAlbums: List<AlbumEntity>) {
        albumDao.addAlbum(
            *listAlbums.map {
                albumCacheMapper.mapToCached(it)
            }.toTypedArray()
        )
    }

    override suspend fun isCached(): Boolean {
        return albumDao.getAlbums().isNotEmpty()
    }

    override suspend fun setLastCacheTime(lastCache: Long) {
        preferencesHelper.lastCacheTime = lastCache
    }

    override suspend fun isExpired(): Boolean {
        val currentTime = System.currentTimeMillis()
        val lastUpdateTime = getLastCacheUpdateTimeMillis()
        return currentTime - lastUpdateTime > EXPIRATION_TIME
    }

    override suspend fun clearAlbums() {
        albumDao.clearAlbums()
    }

    /**
     * Get the last time the cache was accessed.
     */
    private fun getLastCacheUpdateTimeMillis(): Long {
        return preferencesHelper.lastCacheTime
    }

    companion object {
        /**
         * Expiration time set to 10 minutes
         */
        const val EXPIRATION_TIME = (10 * 10 * 1000).toLong()
    }
}