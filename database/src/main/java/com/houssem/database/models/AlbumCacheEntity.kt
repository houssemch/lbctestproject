package com.houssem.database.models

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.houssem.database.utils.DatabaseConstants

@Entity(tableName = DatabaseConstants.ALBUM_TABLE_NAME)
data class AlbumCacheEntity(
    @PrimaryKey
    val id: Int,
    val title: String,
    val thumbnailUrl: String,
)
