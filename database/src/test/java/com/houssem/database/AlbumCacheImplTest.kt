package com.houssem.database

import com.houssem.data.model.AlbumEntity
import com.houssem.database.dao.AlbumDao
import com.houssem.database.mapper.AlbumCacheMapper
import com.houssem.database.models.AlbumCacheEntity
import com.houssem.database.rule.DatabaseDispatcherRule
import com.houssem.database.utils.PreferencesHelper
import kotlinx.coroutines.test.runTest
import org.junit.Assert.assertEquals
import org.junit.Assert.assertFalse
import org.junit.Assert.assertTrue
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito.times
import org.mockito.Mockito.verify
import org.mockito.Mockito.`when`
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class AlbumCacheImplTest {

    @get:Rule
    val mainDispatcherRule = DatabaseDispatcherRule()

    @Mock
    lateinit var albumDao: AlbumDao

    @Mock
    lateinit var albumCacheMapper: AlbumCacheMapper

    @Mock
    lateinit var preferencesHelper: PreferencesHelper

    private lateinit var albumCache: AlbumCacheImpl

    @Before
    fun setup() {
        albumCache = AlbumCacheImpl(albumDao, albumCacheMapper, preferencesHelper)
    }

    @Test
    fun `get albums should return success albums from album dao`() = runTest {
        `when`(albumDao.getAlbums()).thenReturn(testAlbumCacheEntities)
        testAlbumCacheEntities.forEach {
            `when`(albumCacheMapper.mapFromCached(it)).thenReturn(
                AlbumEntity(it.id, it.title, it.thumbnailUrl)
            )
        }
        val albums = albumCache.getAlbums()
        assertEquals(albums.size, 3)
    }

    @Test
    fun `save albums should add album in album dao`() = runTest {
        testAlbumEntities.forEach {
            `when`(albumCacheMapper.mapToCached(it)).thenReturn(
                AlbumCacheEntity(it.id, it.title, it.thumbnailUrl)
            )
        }
        albumCache.saveAlbums(testAlbumEntities)
        verify(albumDao, times(1)).addAlbum(
            testAlbumCacheEntities[0],
            testAlbumCacheEntities[1],
            testAlbumCacheEntities[2]
        )
    }

    @Test
    fun `test isCached return true when albumDao return list of albums`() = runTest {
        `when`(albumDao.getAlbums()).thenReturn(testAlbumCacheEntities)
        assertTrue(albumCache.isCached())
    }

    @Test
    fun `test isCached return false when albumDao return empty list`() = runTest {
        `when`(albumDao.getAlbums()).thenReturn(listOf())
        assertFalse(albumCache.isCached())
    }

    @Test
    fun `test set LastCacheTime is done`() = runTest {
        val lastCacheTime = 679817897L
        albumCache.setLastCacheTime(lastCacheTime)
        verify(preferencesHelper, times(1)).lastCacheTime = lastCacheTime
    }

    @Test
    fun `test clear albums is calling album dao`() = runTest {
        albumCache.clearAlbums()
        verify(albumDao, times(1)).clearAlbums()
    }


}

private val testAlbumCacheEntities = listOf(
    AlbumCacheEntity(1, "Album title 1", "www.fake-url-1.com"),
    AlbumCacheEntity(2, "Album title 2", "www.fake-url-2.com"),
    AlbumCacheEntity(3, "Album title 3", "www.fake-url-3.com"),
)


private val testAlbumEntities = listOf(
    AlbumEntity(1, "Album title 1", "www.fake-url-1.com"),
    AlbumEntity(2, "Album title 2", "www.fake-url-2.com"),
    AlbumEntity(3, "Album title 3", "www.fake-url-3.com"),
)