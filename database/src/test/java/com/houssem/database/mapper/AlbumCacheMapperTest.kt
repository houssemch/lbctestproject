package com.houssem.database.mapper

import com.houssem.data.model.AlbumEntity
import com.houssem.database.models.AlbumCacheEntity
import org.junit.Assert.assertEquals
import org.junit.Test

class AlbumCacheMapperTest {
    private val mapper: AlbumCacheMapper = AlbumCacheMapper()

    @Test
    fun `map AlbumCacheEntity to AlbumEntity object should return AlbumEntity`() {
        val albumCacheEntity = AlbumCacheEntity(
            id = 1,
            title = "title1",
            thumbnailUrl = "www.thumbnail_url.com"
        )
        val albumEntity = mapper.mapFromCached(albumCacheEntity)
        assertEquals(albumEntity.id, 1)
        assertEquals(albumEntity.title, "title1")
        assertEquals(albumEntity.thumbnailUrl, "www.thumbnail_url.com")
    }

    @Test
    fun `map AlbumEntity to AlbumCacheEntity object should return AlbumCacheEntity`() {
        val albumEntity = AlbumEntity(
            id = 1,
            title = "title1",
            thumbnailUrl = "www.thumbnail_url.com"
        )
        val albumCacheEntity = mapper.mapToCached(albumEntity)
        assertEquals(albumCacheEntity.id, 1)
        assertEquals(albumCacheEntity.title, "title1")
        assertEquals(albumCacheEntity.thumbnailUrl, "www.thumbnail_url.com")
    }
}