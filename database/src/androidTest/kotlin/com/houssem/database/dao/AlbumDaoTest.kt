package com.houssem.database.dao

import android.content.Context
import androidx.room.Room.inMemoryDatabaseBuilder
import androidx.test.core.app.ApplicationProvider
import com.houssem.database.database.AlbumsDatabase
import com.houssem.database.models.AlbumCacheEntity
import org.junit.After
import org.junit.Assert.assertEquals

import org.junit.Assert.assertNotNull
import org.junit.Before
import org.junit.Test

class AlbumDaoTest {

    private lateinit var albumDao: AlbumDao
    private lateinit var dataBase: AlbumsDatabase

    private val newAlbumCacheEntities = listOf(
        AlbumCacheEntity(
            id = 1,
            title = "title1",
            thumbnailUrl = "www.thumbnail-url-1.com",
        ),
        AlbumCacheEntity(
            id = 2,
            title = "title2",
            thumbnailUrl = "www.thumbnail-url-2.com",
        ),
        AlbumCacheEntity(
            id = 3,
            title = "title3",
            thumbnailUrl = "www.thumbnail-url-3.com",
        )
    )

    @Before
    fun createDb() {
        val context = ApplicationProvider.getApplicationContext<Context>()
        dataBase = inMemoryDatabaseBuilder(
            context,
            AlbumsDatabase::class.java,
        ).build()
        albumDao = dataBase.cachedAlbumDao()
    }

    @After
    fun closeDb() = dataBase.close()

    @Test
    fun getAlbums_fetch_items_from_database() {

        // save albums entity in database
        newAlbumCacheEntities.forEach {
            albumDao.addAlbum(it)
        }

        val savedAlbumCacheEntities = albumDao.getAlbums()

        val firstSavedAlbumCacheEntity = albumDao.getAlbums()
            .first()

        assertNotNull(savedAlbumCacheEntities)

        assertEquals(
            3,
            savedAlbumCacheEntities.size
        )

        assertEquals(
            1,
            firstSavedAlbumCacheEntity.id
        )
        assertEquals(
            "title1",
            firstSavedAlbumCacheEntity.title
        )
        assertEquals(
            "www.thumbnail-url-1.com",
            firstSavedAlbumCacheEntity.thumbnailUrl
        )
    }
}