package com.houssem.domain.usecase


import com.houssem.domain.model.Album
import com.houssem.domain.repository.AlbumsRepository
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

/**
 * UseCase that retrieves the [Album] list from [AlbumsRepository].
 */
class GetAlbumsUseCase @Inject constructor(
    private val albumsRepository: AlbumsRepository
) : UseCase<List<Album>> {
    override fun apply(): Flow<List<Album>> = albumsRepository.getAlbums()
}
