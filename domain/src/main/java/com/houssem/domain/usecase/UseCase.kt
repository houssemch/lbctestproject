package com.houssem.domain.usecase

import kotlinx.coroutines.flow.Flow

/**
 * Base UseCase interface for implementing Use Cases that returns Flow.
 */
interface UseCase<T> {
    fun apply(): Flow<T>
}