package com.houssem.domain.model

/**
 * Album Domain layer object
 */
data class Album(
    val id: Int,
    val title: String,
    val thumbnailUrl: String
)