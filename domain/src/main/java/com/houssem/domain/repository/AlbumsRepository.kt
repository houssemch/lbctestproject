package com.houssem.domain.repository

import com.houssem.domain.model.Album
import kotlinx.coroutines.flow.Flow

/**
 * Domain layer repository interface to be called by the UseCase.
 */
interface AlbumsRepository {

    /**
     * retrieve list of [Album] as [Flow]
     */
    fun getAlbums(): Flow<List<Album>>

    /**
     * save list of [Album] locally
     */
    suspend fun saveAlbums(listAlbums: List<Album>)
}