package com.houssem.domain.usecase.fakes

import com.houssem.domain.model.Album
import com.houssem.domain.repository.AlbumsRepository
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableSharedFlow

class AlbumsRepositoryFake : AlbumsRepository {

    /**
     * The flow for the list of albums for testing.
     */
    private val albumsFlow: MutableSharedFlow<List<Album>> = MutableSharedFlow()

    override fun getAlbums(): Flow<List<Album>> = albumsFlow

    override suspend fun saveAlbums(listAlbums: List<Album>) {
    }

    /**
     * A test-only API to allow controlling the list of albums from tests.
     */
    suspend fun emitAlbums(albums: List<Album>) {
        albumsFlow.emit(albums)
    }
}