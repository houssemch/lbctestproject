package com.houssem.domain.usecase

import com.houssem.domain.model.Album
import com.houssem.domain.usecase.fakes.AlbumsRepositoryFake
import com.houssem.domain.usecase.rule.DomainDispatcherRule
import kotlinx.coroutines.launch
import kotlinx.coroutines.test.runTest
import org.junit.Assert.assertEquals
import org.junit.Rule
import org.junit.Test

class GetAlbumsUseCaseTest {

    @get:Rule
    val mainDispatcherRule = DomainDispatcherRule()

    private var fakeRepository: AlbumsRepositoryFake = AlbumsRepositoryFake()

    private val useCase: GetAlbumsUseCase = GetAlbumsUseCase(fakeRepository)

    @Test
    fun `apply should return success result with albums list`() = runTest {
        var receivedValue: List<Album> = listOf()
        val collectJob = launch(mainDispatcherRule.testDispatcher) {
            useCase.apply().collect {
                receivedValue = it
            }
        }
        // emit fake albums
        fakeRepository.emitAlbums(testAlbums)
        assertEquals(receivedValue, testAlbums)
        collectJob.cancel()
    }
}

private val testAlbums = listOf(
    Album(1, "Album title 1", "www.fake-url-1.com"),
    Album(2, "Album title 2", "www.fake-url-2.com"),
    Album(3, "Album title 3", "www.fake-url-3.com"),
)