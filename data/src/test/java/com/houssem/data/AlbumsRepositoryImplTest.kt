package com.houssem.data

import com.houssem.data.mapper.AlbumMapper
import com.houssem.data.model.AlbumEntity
import com.houssem.data.repository.AlbumCache
import com.houssem.data.repository.AlbumDataSource
import com.houssem.data.rule.DataDispatcherRule
import com.houssem.data.source.AlbumDataSourceFactory
import kotlinx.coroutines.flow.single
import kotlinx.coroutines.test.runTest
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito.anyList
import org.mockito.Mockito.never
import org.mockito.Mockito.times
import org.mockito.Mockito.verify
import org.mockito.Mockito.`when`
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class AlbumsRepositoryImplTest {

    @get:Rule
    val mainDispatcherRule = DataDispatcherRule()

    @Mock
    lateinit var dataSourceFactory: AlbumDataSourceFactory

    @Mock
    lateinit var albumMapper: AlbumMapper

    @Mock
    lateinit var dataSource: AlbumDataSource

    @Mock
    lateinit var albumCache: AlbumCache

    private lateinit var albumRepository: AlbumsRepositoryImpl


    @Before
    fun setUp() {
        albumRepository = AlbumsRepositoryImpl(dataSourceFactory, albumMapper)
        `when`(dataSourceFactory.getAlbumCache()).thenReturn(albumCache)
        `when`(dataSourceFactory.getCacheDataSource()).thenReturn(dataSource)
    }

    @Test
    fun `get albums with cached true should return albums list from local storage`() = runTest {
        // data has not expired
        `when`(albumCache.isExpired()).thenReturn(false)

        // data is cached
        val isCached = true
        `when`(dataSource.isCached()).thenReturn(isCached)
        `when`(dataSourceFactory.getDataStore(isCached = true, isExpired = false)).thenReturn(
            dataSource
        )

        `when`(dataSource.getAlbums()).thenReturn(testAlbums)

        val albums = albumRepository.getAlbums().single()

        assertEquals(albums.size, 3)
        verify(dataSourceFactory, times(1)).getCacheDataSource()
        verify(dataSource, times(1)).isCached()
        verify(dataSourceFactory, times(1)).getDataStore(isCached = true, isExpired = false)
        verify(dataSource, times(1)).getAlbums()
        verify(dataSource, never()).saveAlbums(anyList())
        testAlbums.forEach {
            verify(albumMapper, times(1)).mapFromEntity(it)
        }
    }

    @Test
    fun `get albums with cached true and expired true should save list locally`() = runTest {
        // data has expired
        `when`(albumCache.isExpired()).thenReturn(true)

        // data is cached
        val isCached = true
        `when`(dataSource.isCached()).thenReturn(isCached)
        `when`(dataSourceFactory.getDataStore(isCached = true, isExpired = true)).thenReturn(
            dataSource
        )

        `when`(dataSource.getAlbums()).thenReturn(testAlbums)

        val albums = albumRepository.getAlbums().single()

        assertEquals(albums.size, 3)
        verify(dataSourceFactory, times(3)).getCacheDataSource()
        verify(dataSource, times(1)).isCached()
        verify(dataSourceFactory, times(1)).getDataStore(isCached = true, isExpired = true)
        verify(dataSource, times(1)).getAlbums()
        verify(dataSource, times(1)).saveAlbums(anyList())
        testAlbums.forEach {
            verify(albumMapper, times(1)).mapFromEntity(it)
        }
    }

    @Test
    fun `get albums with cached false should return from remote and save list locally`() = runTest {
        // data has expired
        `when`(albumCache.isExpired()).thenReturn(false)

        // data is cached
        val isCached = false
        `when`(dataSource.isCached()).thenReturn(isCached)
        `when`(dataSourceFactory.getDataStore(isCached = false, isExpired = false)).thenReturn(
            dataSource
        )

        `when`(dataSource.getAlbums()).thenReturn(testAlbums)

        val albums = albumRepository.getAlbums().single()

        assertEquals(albums.size, 3)
        verify(dataSourceFactory, times(2)).getCacheDataSource()
        verify(dataSource, times(1)).isCached()
        verify(dataSourceFactory, times(1)).getDataStore(isCached = false, isExpired = false)
        verify(dataSource, times(1)).getAlbums()
        verify(dataSource, times(1)).saveAlbums(anyList())
        testAlbums.forEach {
            verify(albumMapper, times(1)).mapFromEntity(it)
        }
    }


}

private val testAlbums = listOf(
    AlbumEntity(1, "Album title 1", "www.fake-url-1.com"),
    AlbumEntity(2, "Album title 2", "www.fake-url-2.com"),
    AlbumEntity(3, "Album title 3", "www.fake-url-3.com"),
)