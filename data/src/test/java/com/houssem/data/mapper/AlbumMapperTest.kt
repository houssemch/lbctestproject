package com.houssem.data.mapper

import com.houssem.data.model.AlbumEntity
import com.houssem.domain.model.Album
import org.junit.Assert.assertEquals
import org.junit.Test

class AlbumMapperTest {

    private val mapper: AlbumMapper = AlbumMapper()

    @Test
    fun `map Album to AlbumEntity object should return AlbumEntity`() {
        val album = Album(
            id = 1,
            title = "title1",
            thumbnailUrl = "www.thumbnail_url.com"
        )
        val albumEntity = mapper.mapToEntity(album)
        assertEquals(albumEntity.id, 1)
        assertEquals(albumEntity.title, "title1")
        assertEquals(albumEntity.thumbnailUrl, "www.thumbnail_url.com")
    }

    @Test
    fun `map AlbumEntity to Album object should return Album`() {
        val albumEntity = AlbumEntity(
            id = 1,
            title = "title1",
            thumbnailUrl = "www.thumbnail_url.com"
        )
        val album = mapper.mapFromEntity(albumEntity)
        assertEquals(album.id, 1)
        assertEquals(album.title, "title1")
        assertEquals(album.thumbnailUrl, "www.thumbnail_url.com")
    }
}