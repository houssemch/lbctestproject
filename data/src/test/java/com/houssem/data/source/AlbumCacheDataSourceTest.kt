package com.houssem.data.source

import com.houssem.data.model.AlbumEntity
import com.houssem.data.repository.AlbumCache
import com.houssem.data.rule.DataDispatcherRule
import kotlinx.coroutines.test.runTest
import org.junit.Assert.assertEquals
import org.junit.Assert.assertFalse
import org.junit.Assert.assertTrue
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito.anyLong
import org.mockito.Mockito.times
import org.mockito.Mockito.verify
import org.mockito.Mockito.`when`
import org.mockito.junit.MockitoJUnitRunner
import java.io.IOException

@RunWith(MockitoJUnitRunner::class)
class AlbumCacheDataSourceTest {

    @get:Rule
    val mainDispatcherRule = DataDispatcherRule()

    @Mock
    lateinit var albumCache: AlbumCache

    private lateinit var cacheDataSource: AlbumCacheDataSource

    @Before
    fun setUp() {
        cacheDataSource = AlbumCacheDataSource(albumCache)
    }

    @Test
    fun `get albums should return albums list from local storage`() = runTest {
        `when`(albumCache.getAlbums()).thenReturn(testAlbums)

        val albums = cacheDataSource.getAlbums()

        assertEquals(albums.size, 3)
        verify(albumCache, times(1)).getAlbums()
    }

    @Test
    fun `get albums should return error`() = runTest {
        `when`(albumCache.getAlbums()).then { throw IOException() }
        var error: Exception? = null
        try {
            cacheDataSource.getAlbums()
        } catch (exception: Exception) {
            error = exception
        }

        assertTrue(error is IOException)
        verify(albumCache, times(1)).getAlbums()
    }

    @Test
    fun `save albums should save albums in local storage`() = runTest {
        cacheDataSource.saveAlbums(testAlbums)

        verify(albumCache, times(1)).saveAlbums(testAlbums)
        verify(albumCache, times(1)).setLastCacheTime(anyLong())
    }

    @Test
    fun `is cahced should return true`() = runTest {
        `when`(albumCache.isCached()).thenReturn(true)
        assertTrue(cacheDataSource.isCached())

        verify(albumCache, times(1)).isCached()
    }

    @Test
    fun `is cached should return false`() = runTest {
        `when`(albumCache.isCached()).thenReturn(false)
        assertFalse(cacheDataSource.isCached())

        verify(albumCache, times(1)).isCached()
    }

    @Test
    fun `test clear albums is calling album cache`() = runTest {
        cacheDataSource.clearAlbums()

        verify(albumCache, times(1)).clearAlbums()
    }
}

private val testAlbums = listOf(
    AlbumEntity(1, "Album title 1", "www.fake-url-1.com"),
    AlbumEntity(2, "Album title 2", "www.fake-url-2.com"),
    AlbumEntity(3, "Album title 3", "www.fake-url-3.com"),
)