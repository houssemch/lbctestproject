package com.houssem.data.source

import com.houssem.data.model.AlbumEntity
import com.houssem.data.repository.AlbumRemote
import com.houssem.data.rule.DataDispatcherRule
import kotlinx.coroutines.test.runTest
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito.times
import org.mockito.Mockito.verify
import org.mockito.Mockito.`when`
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class AlbumRemoteDataSourceTest {

    @get:Rule
    val mainDispatcherRule = DataDispatcherRule()

    @Mock
    lateinit var albumRemote: AlbumRemote

    private lateinit var remoteDataSource: AlbumRemoteDataSource

    @Before
    fun setUp() {
        remoteDataSource = AlbumRemoteDataSource(albumRemote)
    }

    @Test
    fun `get albums should return albums list from remote`() = runTest {
        `when`(albumRemote.getAlbums()).thenReturn(testAlbums)

        val albums = remoteDataSource.getAlbums()

        assertEquals(albums.size, 3)
        verify(albumRemote, times(1)).getAlbums()
    }

    @Test(expected = UnsupportedOperationException::class)
    fun `save albums should return exception`() = runTest {
        remoteDataSource.saveAlbums(testAlbums)
    }

    @Test(expected = UnsupportedOperationException::class)
    fun `isCached should return exception`() = runTest {
        remoteDataSource.isCached()
    }

    @Test(expected = UnsupportedOperationException::class)
    fun `clearAlbums should return exception`() = runTest {
        remoteDataSource.clearAlbums()
    }
}

private val testAlbums = listOf(
    AlbumEntity(1, "Album title 1", "www.fake-url-1.com"),
    AlbumEntity(2, "Album title 2", "www.fake-url-2.com"),
    AlbumEntity(3, "Album title 3", "www.fake-url-3.com"),
)
