package com.houssem.data.source

import com.houssem.data.repository.AlbumCache
import com.houssem.data.rule.DataDispatcherRule
import org.junit.Assert.assertTrue
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class AlbumDataSourceFactoryTest {

    @get:Rule
    val mainDispatcherRule = DataDispatcherRule()

    @Mock
    lateinit var remoteDataSource: AlbumRemoteDataSource

    @Mock
    lateinit var cacheDataSource: AlbumCacheDataSource

    @Mock
    lateinit var albumCache: AlbumCache

    private lateinit var dataSourceFactory: AlbumDataSourceFactory

    @Before
    fun setUp() {
        dataSourceFactory = AlbumDataSourceFactory(remoteDataSource, cacheDataSource, albumCache)
    }

    @Test
    fun `get data store with cache and not expired should return albums from cache data source`() {
        val dataSource = dataSourceFactory.getDataStore(isCached = true, isExpired = false)
        assertTrue(
            dataSource is AlbumCacheDataSource
        )
    }

    @Test
    fun `get data store with cache and expired should return albums from remote data source`() {
        val dataSource = dataSourceFactory.getDataStore(isCached = true, isExpired = true)
        assertTrue(
            dataSource is AlbumRemoteDataSource
        )
    }

    @Test
    fun `get data store without cache and not expired should return albums from remote data source`() {
        val dataSource = dataSourceFactory.getDataStore(isCached = false, isExpired = false)
        assertTrue(
            dataSource is AlbumRemoteDataSource
        )
    }

    @Test
    fun `get data store without cache and expired should return albums from remote data source`() {
        val dataSource = dataSourceFactory.getDataStore(isCached = false, isExpired = true)
        assertTrue(
            dataSource is AlbumRemoteDataSource
        )
    }
}