package com.houssem.data.mapper

import com.houssem.data.model.AlbumEntity
import com.houssem.domain.model.Album

class AlbumMapper : Mapper<AlbumEntity, Album> {

    override fun mapFromEntity(type: AlbumEntity): Album {
        return with(type) {
            Album(
                id = id,
                title = title,
                thumbnailUrl = thumbnailUrl
            )
        }
    }


    override fun mapToEntity(type: Album): AlbumEntity {
        return with(type) {
            AlbumEntity(
                id = id,
                title = title,
                thumbnailUrl = thumbnailUrl
            )
        }
    }
}
