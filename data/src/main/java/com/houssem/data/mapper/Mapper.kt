package com.houssem.data.mapper

/**
 * Base mapper interface for converting from and to Entity object.
 */
interface Mapper<E, D> {

    fun mapFromEntity(type: E): D

    fun mapToEntity(type: D): E
}