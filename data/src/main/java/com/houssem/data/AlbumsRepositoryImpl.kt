package com.houssem.data

import com.houssem.data.mapper.AlbumMapper
import com.houssem.data.source.AlbumDataSourceFactory
import com.houssem.domain.model.Album
import com.houssem.domain.repository.AlbumsRepository
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import javax.inject.Inject

/**
 * Implementation of [AlbumsRepository] defined in Domain layer, that retrieves data
 * either from remote or local data source.
 */
class AlbumsRepositoryImpl @Inject constructor(
    private val dataSourceFactory: AlbumDataSourceFactory,
    private val albumMapper: AlbumMapper,
) : AlbumsRepository {

    override fun getAlbums(): Flow<List<Album>> = flow {
        val isExpired = dataSourceFactory.getAlbumCache().isExpired()
        if (isExpired) {
            // Clear albums from database when expiration duration is reached.
            dataSourceFactory.getCacheDataSource().clearAlbums()
        }

        val isCached = dataSourceFactory.getCacheDataSource().isCached()
        // Get the data source from which data should be retrieved depending on isCached and isExpired values.
        val albumsDataSource = dataSourceFactory.getDataStore(isCached, isExpired)
        val albumsList =
            albumsDataSource.getAlbums().map { albumEntity ->
                albumMapper.mapFromEntity(albumEntity)
            }

        // Save albums locally each time we retrieve data from remote source. if they are not already saved or expired.
        if (!isCached || isExpired) {
            saveAlbums(albumsList)
        }

        emit(albumsList)
    }

    override suspend fun saveAlbums(listAlbums: List<Album>) {
        val albumsEntities = listAlbums.map { album ->
            albumMapper.mapToEntity(album)
        }
        dataSourceFactory.getCacheDataSource().saveAlbums(albumsEntities)
    }
}
