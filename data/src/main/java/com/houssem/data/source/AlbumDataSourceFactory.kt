package com.houssem.data.source

import com.houssem.data.repository.AlbumCache
import com.houssem.data.repository.AlbumDataSource
import javax.inject.Inject

/**
 * Factory class that creates the remote and local data sources classes.
 */
class AlbumDataSourceFactory @Inject constructor(
    private val remoteDataSource: AlbumRemoteDataSource,
    private val cacheDataSource: AlbumCacheDataSource,
    private val albumCache: AlbumCache
) {

    fun getDataStore(isCached: Boolean, isExpired: Boolean): AlbumDataSource {
        return if (isCached && !isExpired) {
            return getCacheDataSource()
        } else {
            getRemoteDataSource()
        }
    }

    private fun getRemoteDataSource(): AlbumDataSource {
        return remoteDataSource
    }

    fun getCacheDataSource(): AlbumDataSource {
        return cacheDataSource
    }

    fun getAlbumCache(): AlbumCache {
        return albumCache
    }
}
