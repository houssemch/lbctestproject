package com.houssem.data.source

import com.houssem.data.model.AlbumEntity
import com.houssem.data.repository.AlbumDataSource
import com.houssem.data.repository.AlbumRemote
import javax.inject.Inject

/**
 * Data layer class that retrieves data from remote data source through [AlbumRemote] interface.
 */
class AlbumRemoteDataSource @Inject constructor(
    private val albumRemote: AlbumRemote
) : AlbumDataSource {
    override suspend fun getAlbums(): List<AlbumEntity> {
        return albumRemote.getAlbums()
    }

    override suspend fun saveAlbums(listAlbums: List<AlbumEntity>) {
        throw UnsupportedOperationException("Save albums is not supported for RemoteDataSource.")
    }

    override suspend fun isCached(): Boolean {
        throw UnsupportedOperationException("Cache is not supported for RemoteDataSource")
    }

    override suspend fun clearAlbums() {
        throw UnsupportedOperationException("clearAlbums is not supported for RemoteDataSource")
    }
}