package com.houssem.data.source

import com.houssem.data.model.AlbumEntity
import com.houssem.data.repository.AlbumCache
import com.houssem.data.repository.AlbumDataSource
import javax.inject.Inject

class AlbumCacheDataSource @Inject constructor(
    private val albumCache: AlbumCache
) : AlbumDataSource {
    override suspend fun getAlbums(): List<AlbumEntity> {
        return albumCache.getAlbums()
    }

    override suspend fun saveAlbums(listAlbums: List<AlbumEntity>) {
        albumCache.saveAlbums(listAlbums)
        albumCache.setLastCacheTime(System.currentTimeMillis())
    }

    override suspend fun isCached(): Boolean {
        return albumCache.isCached()
    }

    override suspend fun clearAlbums() {
        albumCache.clearAlbums()
    }
}
