package com.houssem.data.model

/**
 * Album data layer object
 */
data class AlbumEntity(
    val id: Int,
    val title: String,
    val thumbnailUrl: String
)