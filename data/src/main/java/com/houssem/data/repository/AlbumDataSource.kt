package com.houssem.data.repository

import com.houssem.data.model.AlbumEntity

/**
 * Defines functions that handles data from remote and local sources.
 */
interface AlbumDataSource {
    /**
     * Retrieve data from remote and local sources
     */
    suspend fun getAlbums(): List<AlbumEntity>

    /**
     * Save data in local source
     */
    suspend fun saveAlbums(listAlbums: List<AlbumEntity>)

    /**
     * Check if data was saved locally
     */
    suspend fun isCached(): Boolean

    /**
     * Delete all albums saved locally.
     */
    suspend fun clearAlbums()
}