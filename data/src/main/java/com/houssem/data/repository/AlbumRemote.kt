package com.houssem.data.repository

import com.houssem.data.model.AlbumEntity

/**
 * Data layer interface that defines functions to retrieves data from remote source,
 * it should be implemented by the Remote module.
 */
interface AlbumRemote {

    /**
     * Retrieves data from remote source, it should be implemented
     * by the Remote module.
     */
    suspend fun getAlbums(): List<AlbumEntity>
}