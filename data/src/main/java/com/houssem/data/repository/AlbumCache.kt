package com.houssem.data.repository

import com.houssem.data.model.AlbumEntity

/**
 * Repository layer interface to define Cache functions.
 */
interface AlbumCache {

    /**
     * Retrieve list of [AlbumEntity] from local database.
     */
    suspend fun getAlbums(): List<AlbumEntity>

    /**
     * Save list of [AlbumEntity] in the local database.
     * @param listAlbums: the list of AlbumsEntity retrieved from
     * the remote source to be saved locally.
     */
    suspend fun saveAlbums(listAlbums: List<AlbumEntity>)

    /**
     * Check if data is cached locally
     */
    suspend fun isCached(): Boolean

    /**
     * Save locally the last time of saving data locally.
     */
    suspend fun setLastCacheTime(lastCache: Long)

    /**
     * Check if data saved locally is expired or not.
     */
    suspend fun isExpired(): Boolean

    /**
     * Delete the list of albums saved locally.
     */
    suspend fun clearAlbums()
}