package com.houssem.presentation.utils.connectivity

import androidx.compose.runtime.Composable
import androidx.compose.runtime.State
import androidx.compose.runtime.produceState
import androidx.compose.ui.platform.LocalContext
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.FlowPreview
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.flow.debounce

@OptIn(FlowPreview::class)
@ExperimentalCoroutinesApi
@Composable
fun connectivityState(): State<Boolean> {
    val context = LocalContext.current

    // Creates a State<Boolean> with current connectivity state as initial value
    return produceState(initialValue = context.isCurrentlyConnected()) {
        context.observeConnectivity().debounce(500)
            .collectLatest {
                value = it
            }
    }
}