package com.houssem.presentation.utils.error

import androidx.compose.runtime.Composable
import androidx.compose.ui.res.stringResource
import com.houssem.presentation.R

object ErrorCodes {
    const val ERROR_GENERAL = 1000
    const val ERROR_TIME_OUT = 1001
    const val ERROR_NO_INTERNET = 1002
}

@Composable
fun getErrorMessage(errorCode: Int?): String {
    return when (errorCode) {
        ErrorCodes.ERROR_GENERAL -> stringResource(
            id = R.string.error_general
        )

        ErrorCodes.ERROR_NO_INTERNET -> stringResource(
            id = R.string.error_no_internet
        )

        ErrorCodes.ERROR_TIME_OUT -> stringResource(
            id = R.string.error_timeout
        )

        else -> stringResource(
            id = R.string.error_general
        )
    }
}