package com.houssem.presentation.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.houssem.domain.model.Album
import com.houssem.domain.usecase.UseCase
import com.houssem.presentation.mapper.AlbumUiMapper
import com.houssem.presentation.result.asResult
import com.houssem.presentation.result.Result
import com.houssem.presentation.state.AlbumsListState
import com.houssem.presentation.utils.error.ErrorCodes
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.distinctUntilChanged
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.launch
import java.net.SocketTimeoutException
import java.net.UnknownHostException
import javax.inject.Inject

@HiltViewModel
class AlbumsListViewModel @Inject constructor(
    private val getAlbumsUseCase: UseCase<List<Album>>,
    private val albumUiMapper: AlbumUiMapper,
    private val dispatcher: CoroutineDispatcher
) : ViewModel() {

    init {
        this.getAlbumsList()
    }

    private val _albumsStateFlow = MutableStateFlow<AlbumsListState>(AlbumsListState.Loading)
    val albumsStateFlow: StateFlow<AlbumsListState> = _albumsStateFlow

    fun getAlbumsList() {
        viewModelScope.launch(dispatcher) {
            getAlbumsUseCase.apply()
                .asResult()
                .map { result ->
                    produceState(result)
                }
                .distinctUntilChanged()
                .collect { _albumsStateFlow.emit(it) }
        }
    }

    /**
     * Produce [AlbumsListState] to be used for the display based on the received [Result]
     */
    private fun produceState(result: Result<List<Album>>): AlbumsListState {
        return when (result) {
            is Result.Success -> {
                val data = result.data
                if (data.isEmpty()) {
                    AlbumsListState.AlbumsEmptyList
                } else {
                    AlbumsListState.AlbumsSuccessList(data.map {
                        albumUiMapper.mapToUiObject(it)
                    })
                }
            }

            is Result.Loading -> AlbumsListState.Loading
            is Result.Error -> {
                val errorCode = when (result.exception) {
                    is UnknownHostException -> ErrorCodes.ERROR_NO_INTERNET
                    is SocketTimeoutException -> ErrorCodes.ERROR_TIME_OUT
                    else -> ErrorCodes.ERROR_GENERAL
                }
                AlbumsListState.AlbumsErrorList(errorCode)
            }
        }
    }
}