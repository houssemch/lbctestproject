package com.houssem.presentation.state

/**
 * Sealed interface that defines the view states of AlbumsList screen
 */
sealed interface AlbumsListState {

    data object Loading : AlbumsListState

    data class AlbumsSuccessList(
        val albums: List<AlbumUi>
    ) : AlbumsListState

    data object AlbumsEmptyList : AlbumsListState

    class AlbumsErrorList(
        val errorCode: Int
    ) : AlbumsListState
}