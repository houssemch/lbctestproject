package com.houssem.presentation.state

import androidx.compose.runtime.Stable

@Stable
data class AlbumUi(
    val id: Int,
    val title: String,
    val thumbnailUrl: String
)