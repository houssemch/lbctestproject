package com.houssem.presentation.mapper

import com.houssem.domain.model.Album
import com.houssem.presentation.state.AlbumUi
import javax.inject.Inject

class AlbumUiMapper @Inject constructor() {

    fun mapToUiObject(album: Album): AlbumUi {
        return with(album) {
            AlbumUi(
                id = id,
                title = title,
                thumbnailUrl = thumbnailUrl
            )
        }
    }
}