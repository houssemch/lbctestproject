package com.houssem.presentation.designsystem.component.loader

import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.wrapContentSize
import androidx.compose.material3.CircularProgressIndicator
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp

/**
 * Display circular progress loader.
 */
@Composable
fun CircularLoader(modifier: Modifier = Modifier, size: Dp) {
    Box(
        modifier = modifier
            .fillMaxSize()
            .wrapContentSize(Alignment.Center)
    ) {
        CircularProgressIndicator(
            modifier = Modifier
                .size(size),
            strokeWidth = 3.dp,
            color = Color.Black
        )
    }
}

@Preview
@Composable
fun CircularLoaderPreview() {
    CircularLoader(size = 60.dp)
}