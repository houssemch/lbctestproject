package com.houssem.presentation.designsystem.theme

import androidx.compose.ui.graphics.Color

val LightGrey = Color(0xFFEDEDED)