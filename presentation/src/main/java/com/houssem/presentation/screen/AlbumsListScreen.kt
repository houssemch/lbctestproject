package com.houssem.presentation.screen

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.wrapContentHeight
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.LazyListState
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.lazy.rememberLazyListState
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.Card
import androidx.compose.material3.CardDefaults
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Scaffold
import androidx.compose.material3.SnackbarDuration
import androidx.compose.material3.SnackbarHost
import androidx.compose.material3.SnackbarHostState
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.lifecycle.compose.collectAsStateWithLifecycle
import coil.compose.SubcomposeAsyncImage
import coil.request.ImageRequest
import com.houssem.presentation.R
import com.houssem.presentation.designsystem.component.error.ErrorScreen
import com.houssem.presentation.designsystem.component.loader.CircularLoader
import com.houssem.presentation.designsystem.component.topbar.AppTopBar
import com.houssem.presentation.state.AlbumsListState
import com.houssem.presentation.designsystem.theme.LightGrey
import com.houssem.presentation.state.AlbumUi
import com.houssem.presentation.utils.connectivity.connectivityState
import com.houssem.presentation.utils.error.ErrorCodes.ERROR_GENERAL
import com.houssem.presentation.utils.error.getErrorMessage
import com.houssem.presentation.viewmodel.AlbumsListViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi

@OptIn(ExperimentalCoroutinesApi::class)
@Composable
fun AlbumsListScreen(
    albumsViewModel: AlbumsListViewModel = hiltViewModel(),
) {
    // State of the network connection
    val isConnected by connectivityState()

    // State of the albums List
    val albumsListState: AlbumsListState by albumsViewModel.albumsStateFlow.collectAsStateWithLifecycle()

    // If the user doesn't have a network connection, we show a snack bar to inform him.
    val snackBarHostState = remember { SnackbarHostState() }
    val notConnectedMessage = stringResource(R.string.no_network)
    val isOffline = !isConnected

    LaunchedEffect(isOffline) {
        if (isOffline) {
            snackBarHostState.showSnackbar(
                message = notConnectedMessage,
                duration = SnackbarDuration.Indefinite,
            )
        } else if (albumsListState is AlbumsListState.AlbumsErrorList) {
            // In case of error, we retrieve again the data when the network is back.
            albumsViewModel.getAlbumsList()
        }
    }

    Scaffold(
        topBar = { AppTopBar(stringResource(id = R.string.albums)) },
        snackbarHost = { SnackbarHost(snackBarHostState) },
        content = {
            AlbumsScreenContent(
                albumsListState = albumsListState,
                modifier = Modifier
                    .padding(it),
            )
        }
    )
}

@Composable
fun AlbumsScreenContent(
    albumsListState: AlbumsListState,
    modifier: Modifier = Modifier,
) {
    Column(
        modifier = modifier,
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        when (albumsListState) {
            AlbumsListState.Loading -> {
                CircularLoader(modifier = modifier, size = 60.dp)
            }

            is AlbumsListState.AlbumsEmptyList -> {
                ErrorScreen(
                    message = stringResource(
                        id = R.string.emptyList
                    )
                )
            }

            is AlbumsListState.AlbumsErrorList -> {
                val errorMessage = getErrorMessage(
                    albumsListState.errorCode
                )
                ErrorScreen(
                    message = errorMessage
                )
            }

            is AlbumsListState.AlbumsSuccessList -> {
                AlbumsList(
                    listState = rememberLazyListState(),
                    albumsList = albumsListState.albums
                )
            }
        }
    }
}

@Composable
fun AlbumsList(
    listState: LazyListState,
    albumsList: List<AlbumUi>,
    modifier: Modifier = Modifier
) {

    LazyColumn(
        modifier = modifier
            .wrapContentHeight()
            .background(Color.White),
        contentPadding = PaddingValues(horizontal = 16.dp, vertical = 8.dp),
        verticalArrangement = Arrangement.spacedBy(8.dp),
        state = listState,
    ) {
        items(albumsList, key = { it.id }) { album ->
            AlbumItem(
                album = album
            )
        }
    }
}

@Composable
fun AlbumItem(
    album: AlbumUi,
    modifier: Modifier = Modifier,
) {
    Card(
        elevation = CardDefaults.cardElevation(
            defaultElevation = 2.dp
        ),
        modifier = modifier
            .fillMaxWidth(),
        shape = RoundedCornerShape(12.dp),
        colors = CardDefaults.cardColors(containerColor = LightGrey)
    ) {
        Row(
            verticalAlignment = Alignment.CenterVertically,
            modifier = Modifier
                .padding(8.dp),
        ) {

            // Build an ImageRequest with Coil
            val imageRequest = imageRequest(album)

            // Load and display the image with SubcomposeAsyncImage
            SubcomposeAsyncImage(
                model = imageRequest,
                contentDescription = null,
                modifier = Modifier
                    .size(72.dp)
                    .padding(horizontal = 12.dp),
                contentScale = ContentScale.Fit,
                loading = {
                    CircularLoader(modifier = modifier, size = 32.dp)
                }
            )

            Text(
                modifier = Modifier
                    .padding(4.dp),
                text = album.title,
                style = MaterialTheme.typography.labelMedium,
                color = Color.Black,
                textAlign = TextAlign.Start
            )
        }
    }
}

@Composable
private fun imageRequest(album: AlbumUi) = ImageRequest.Builder(LocalContext.current)
    .data(album.thumbnailUrl)
    .dispatcher(Dispatchers.IO)
    .memoryCacheKey(album.thumbnailUrl)
    .diskCacheKey(album.thumbnailUrl)
    .placeholder(R.drawable.ic_launcher_background)
    .error(R.drawable.ic_launcher_background)
    .fallback(R.drawable.ic_launcher_background)
    .build()

@Preview(showBackground = true)
@Composable
fun AlbumsScreenLoading() {
    AlbumsScreenContent(AlbumsListState.Loading)
}

@Preview(showBackground = true)
@Composable
fun AlbumsScreenError() {
    AlbumsScreenContent(AlbumsListState.AlbumsErrorList(ERROR_GENERAL))
}

@Preview(showBackground = true)
@Composable
fun AlbumsScreenWithAlbumsList() {
    val albums = listOf(
        AlbumUi(
            1,
            "accusamus beatae ad facilis cum similique qui sunt",
            "https://via.placeholder.com/150/92c952"
        ),
        AlbumUi(
            2,
            "reprehenderit est deserunt velit ipsam",
            "https://via.placeholder.com/150/771796"
        ),
        AlbumUi(
            3,
            "officia porro iure quia iusto qui ipsa ut modi",
            "https://via.placeholder.com/150/24f355"
        )
    )
    AlbumsScreenContent(AlbumsListState.AlbumsSuccessList(albums))
}
