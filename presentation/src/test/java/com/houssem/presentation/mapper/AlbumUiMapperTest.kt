package com.houssem.presentation.mapper

import com.houssem.domain.model.Album
import org.junit.Assert.assertEquals
import org.junit.Test

class AlbumUiMapperTest {

    private val mapper: AlbumUiMapper = AlbumUiMapper()

    @Test
    fun `map Album to Ui object should return AlbumUi object`() {
        val album = Album(
            id = 1,
            title = "title1",
            thumbnailUrl = "www.thumbnail_url.com"
        )
        val albumUiIObject = mapper.mapToUiObject(album)
        assertEquals(albumUiIObject.id, 1)
        assertEquals(albumUiIObject.title, "title1")
        assertEquals(albumUiIObject.thumbnailUrl, "www.thumbnail_url.com")
    }
}