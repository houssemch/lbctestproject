package com.houssem.presentation.result

import app.cash.turbine.test
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.test.runTest
import org.junit.Assert.assertEquals
import org.junit.Test

class ResultTest {

    @Test
    fun testResult_Loading_Success_Exception() = runTest {
        flow {
            emit("test value")
            throw Exception("test Exception")
        }
            .asResult()
            .test {
                assertEquals(Result.Loading, awaitItem())

                assertEquals(Result.Success("test value"), awaitItem())

                assertEquals(
                    "test Exception",
                    (awaitItem() as Result.Error).exception.message,
                )
                awaitComplete()
            }
    }
}