package com.houssem.presentation.fakes

import com.houssem.domain.model.Album
import com.houssem.domain.usecase.UseCase
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableSharedFlow

class GetAlbumsUseCaseFake: UseCase<List<Album>> {

    /**
     * The flow for the list of albums for testing.
     */
    private val albumsFlow: MutableSharedFlow<List<Album>> = MutableSharedFlow()

     override fun apply(): Flow<List<Album>> = albumsFlow


    /**
     * A test-only API to allow controlling the list of albums from tests.
     */
    suspend fun emitAlbums(albums: List<Album>) {
        albumsFlow.emit(albums)
    }
}