package com.houssem.presentation.viewmodel

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.houssem.domain.model.Album
import com.houssem.presentation.fakes.GetAlbumsUseCaseFake
import com.houssem.presentation.mapper.AlbumUiMapper
import com.houssem.presentation.rule.PresentationDispatcherRule
import com.houssem.presentation.state.AlbumsListState
import kotlinx.coroutines.launch
import kotlinx.coroutines.test.runTest
import org.junit.Assert.assertEquals
import org.junit.Assert.assertTrue
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TestRule
import org.junit.runner.RunWith
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class AlbumsListViewModelTest {

    @get:Rule
    val testInstantTaskExecutorRule: TestRule = InstantTaskExecutorRule()

    @get:Rule
    val dispatcherRule = PresentationDispatcherRule()

    private var getAlbumsUseCase: GetAlbumsUseCaseFake = GetAlbumsUseCaseFake()

    private lateinit var viewModel: AlbumsListViewModel

    private val receivedUiStates = mutableListOf<AlbumsListState>()

    @Before
    fun setup() {
        viewModel = AlbumsListViewModel(
            getAlbumsUseCase = getAlbumsUseCase,
            albumUiMapper = AlbumUiMapper(),
            dispatcher = dispatcherRule.testDispatcher
        )
    }

    @Test
    fun `albumsStateFlow should return loading then emptyList state when result is empty`() =
        runTest {
            viewModel.getAlbumsList()
            val collectJob = launch(dispatcherRule.testDispatcher) {
                viewModel.albumsStateFlow.collect {
                    receivedUiStates.add(it)
                }
            }
            // emit fake albums
            getAlbumsUseCase.emitAlbums(listOf())
            assertEquals(receivedUiStates[0], AlbumsListState.Loading)
            assertTrue(receivedUiStates[1] is AlbumsListState.AlbumsEmptyList)

            collectJob.cancel()
        }

    @Test
    fun `albumsStateFlow should return loading then success state when result is a list of albums`() = runTest {
        viewModel.getAlbumsList()
        val collectJob = launch(dispatcherRule.testDispatcher) {
            viewModel.albumsStateFlow.collect {
                receivedUiStates.add(it)
            }
        }
        // emit fake albums
        getAlbumsUseCase.emitAlbums(testAlbums)
        assertEquals(receivedUiStates[0], AlbumsListState.Loading)
        assertTrue(receivedUiStates[1] is AlbumsListState.AlbumsSuccessList)

        collectJob.cancel()
    }
}

private val testAlbums = listOf(
    Album(1, "Album title 1", "www.fake-url-1.com"),
    Album(2, "Album title 2", "www.fake-url-2.com"),
    Album(3, "Album title 3", "www.fake-url-3.com"),
)